"""Common storage tests."""

import io

import pytest
import requests

from fw_storage import FileNotFound, create_storage_client, create_storage_config
from fw_storage.storage import create_storage_client_cls

from .conftest import create_files


@pytest.fixture
def storage_url(request, prepare_url):
    return prepare_url(request.param)


@pytest.fixture
def storage(storage_url):
    return create_storage_client(storage_url)


def test_create_storage_client_raises_on_invalid_url():
    with pytest.raises(ValueError):
        create_storage_client("foo")


def test_create_storage_config_raises_on_invalid_url():
    with pytest.raises(ValueError):
        create_storage_config("foo")


def test_create_storage_client_raises_on_invalid_scheme():
    with pytest.raises(ValueError):
        create_storage_client("test://")


def test_create_storage_config_raises_on_invalid_scheme():
    with pytest.raises(ValueError):
        create_storage_config("test://")


def test_create_storage_client_cls_raises_on_invalid_import_path():
    with pytest.raises(ValueError):
        create_storage_client_cls("test.module.TestStorage")


def test_storage_cleanup(tmp_path, mocker):
    with create_storage_client(f"fs://{tmp_path}") as fs:
        cleanup = mocker.patch.object(fs, "cleanup")
    assert cleanup.called


def test_read_write_perms(storage):
    create_files(storage, "foo")
    storage.test_read()
    storage.test_write()


def test_ls(storage):
    create_files(storage, "a", "c", "e", "b/a/a", "b/A", "b/1", "b/b", "d/a")
    expected = ["a", "b/1", "b/A", "b/a/a", "b/b", "c", "d/a", "e"]
    assert [f.path for f in storage.ls()] == expected


def test_ls_with_prefix_including_special_characters(storage):
    if storage.config.type == "fs":
        pytest.skip("no storage prefix in fs")
    create_files(storage, "b +A/a")
    storage.config.prefix += "/b +A"
    expected = ["a"]
    assert [f.path for f in storage.ls()] == expected


def test_ls_filter(storage):
    create_files(storage, "foo/a.txt", ("foo/b.dat", ""), "bar/c.txt", "bar/d.dat")
    expected = ["foo/a.txt"]

    include = ["path=~foo/a"]
    assert [f.path for f in storage.ls(include=include)] == expected

    exclude = ["size=0", "path=~bar"]
    assert [f.path for f in storage.ls(exclude=exclude)] == expected

    assert [f.path for f in storage.ls(include=include, exclude=exclude)] == expected


def test_stat(storage):
    create_files(storage, "foo/bar")
    fileinfo = storage.stat("foo/bar")
    assert fileinfo.path == "foo/bar"
    assert fileinfo.size == 7


def test_stat_file_not_found(storage):
    with pytest.raises(FileNotFound):
        storage.stat("foo/bar")


def test_get(storage):
    create_files(storage, ("foo/bar", "test"))
    with storage.get("foo/bar") as file:
        assert file.metapath == "foo/bar"
        assert file.read() == b"test"
    # works with fileinfo
    with storage.get(next(storage.ls())) as file:
        assert file.metapath == "foo/bar"
        assert file.read() == b"test"


def test_get_wrong_path(storage):
    with pytest.raises(FileNotFound):
        storage.get("foo.txt")


def test_set(storage):
    storage.set("foo/bar.txt", io.BytesIO(b"test"))
    with storage.get("foo/bar.txt") as file:
        assert file.read() == b"test"
    # works with fileinfo
    fileinfo = next(storage.ls())
    storage.set(fileinfo, io.BytesIO(b"test_info"))
    with storage.get(fileinfo) as file:
        assert file.read() == b"test_info"


def test_set_from_path(storage, tmp_path):
    file_path = tmp_path / "foo.txt"
    file_path.write_text("foo")
    storage.set("foo.txt", file_path)
    with storage.get("foo.txt") as file:
        assert file.read() == b"foo"
    storage.set("foo.txt", str(file_path))
    with storage.get("foo.txt") as file:
        assert file.read() == b"foo"


def test_set_from_stream(storage, http_testserver):
    http_testserver.add_callback("/test/stream", lambda: (b"foo", 200))
    resp = requests.get(f"{http_testserver.url}/test/stream", stream=True, timeout=0.1)
    storage.set("foo.txt", resp.raw)
    with storage.get("foo.txt") as file:
        assert file.read() == b"foo"


def test_rm(storage):
    create_files(storage, "foo/bar")
    storage.rm(next(storage.ls()))
    # make sure to cleanup if storage does rm in batches
    storage.cleanup()
    with pytest.raises(FileNotFound, match=r".*foo/bar.*"):
        storage.get("foo/bar")


def test_rm_recurse(storage):
    create_files(storage, "foo", "bar/a", "bar/b")
    storage.rm("bar", recurse=True)
    # make sure to cleanup if storage does rm in batches
    storage.cleanup()
    assert [f.path for f in storage.ls()] == ["foo"]


def test_signed_url_upload_single(storage):
    if storage.config.type == "fs":
        pytest.skip("signed url is not supported in fs")
    url = storage.generate_upload_url("foo")
    response = requests.put(url, data=b"foo", headers={"x-ms-blob-type": "BlockBlob"})
    assert response.ok
    with storage.get("foo") as file:
        assert file.read() == b"foo"


def test_signed_url_upload_multipart(storage):
    if storage.config.type == "fs":
        pytest.skip("signed url is not supported in fs")
    # init
    mpu_id = storage.initiate_multipart_upload("foo")
    # upload parts
    url = storage.generate_upload_url("foo", mpu_id, 1)
    response = requests.put(url, data=b"foo", headers={"x-ms-blob-type": "BlockBlob"})
    assert response.ok
    etag = response.headers.get("ETag")
    # complete
    storage.complete_multipart_upload("foo", mpu_id, [{"part": 1, "etag": etag}])
    # validate content
    with storage.get("foo") as file:
        assert file.read() == b"foo"


def test_signed_url_download(storage):
    if storage.config.type == "fs":
        pytest.skip("signed url is not supported in fs")
    create_files(storage, "foo")
    url = storage.generate_download_url("foo")
    response = requests.get(url)
    assert response.ok
    assert response.content == b"foo"
