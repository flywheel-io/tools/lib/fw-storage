import pytest

from fw_storage.future import create_storage_config
from fw_storage.future.base import File
from fw_storage.future.utils import URL

url_str = "scheme+driver://user:pass@host:123/path?a=1&b=2#fragment"  # gitleaks:allow
url_dict = {
    "scheme": "scheme",
    "driver": "driver",
    "username": "user",
    "password": "pass",
    "host": "host",
    "port": 123,
    "path": "/path",  # path contains the separator slash
    "fragment": "fragment",
    "a": "1",  # query params (de)serialized as extras
    "b": "2",
}


def test_url():
    assert URL(**url_dict) == URL.from_string(url_str)
    assert url_str == str(URL(**url_dict))
    assert url_dict == URL.from_string(url_str).model_dump()
    with pytest.raises(DeprecationWarning, match=".*dict.*is deprecated"):
        assert url_dict == URL.from_string(url_str).dict()


def test_invalid_url():
    with pytest.raises(ValueError, match="cannot parse url"):
        URL.from_string("foo")


def test_invalid_storage_url():
    with pytest.raises(ValueError, match="invalid storage url scheme"):
        create_storage_config("foo://")


def test_file_backward_compat():
    file = File(type="gs", path="path", size=1, hash="hash", created=1, modified=2)
    assert file.ctime == 1
    assert file.mtime == 2
