import os
import stat
from functools import partial
from pathlib import Path
from unittest.mock import call

import pytest

from fw_storage.future import create_storage_client, create_storage_config, errors
from fw_storage.future.types.fs import FSConfigOverride

cwd = Path.cwd()
home = Path.home()
root = os.geteuid() == 0
urls = [
    ("fs:///abs", "fs:///abs", "fs:///abs"),  # absolute paths
    ("fs:///", "fs:///", "fs:///"),  # root edge-case
    ("fs://", f"fs://{cwd}", f"fs://{cwd}"),  # relative paths
    ("fs://.", f"fs://{cwd}", f"fs://{cwd}"),
    ("fs://rel", f"fs://{cwd}/rel", f"fs://{cwd}/rel"),
    ("fs://~", f"fs://{home}", f"fs://{home}"),  # user home
    ("fs:///?cleanup_dirs=0", "fs:///", "fs:///"),  # bool load, exclude default
    ("fs:///?content_hash=1", "fs:///", "fs:///?content_hash=true"),  # bool dump
    ("fs:///?follow_links=1", "fs:///", "fs:///?follow_links=true"),
    ("fs:///?groups=", "fs:///", "fs:///?groups="),  # explicit empty groups []
    ("fs:///?groups=123", "fs:///", "fs:///?groups=123"),  # a single groups
    ("fs:///?groups=123,456", "fs:///", "fs:///?groups=123,456"),  # multi groups
    ("fs:///?chown=123", "fs:///", "fs:///?chown=123"),  # user only
    ("fs:///?chown=123:456", "fs:///", "fs:///?chown=123:456"),  # user and group
    ("fs:///?chmod=rw-r-----", "fs:///", "fs:///?chmod=640"),  # rwx notation
    ("fs:///?chmod=640", "fs:///", "fs:///?chmod=640"),  # octal notation
    ("fs:///?chmod=600&chown=123", "fs:///", "fs:///?chown=123&chmod=600"),  # &sort
]


@pytest.mark.parametrize("url_in, safe_url, full_url", urls)
def test_fs_config_factory(url_in, safe_url, full_url):
    """Test fs config full/safe urls, covering all supported fields."""
    config = create_storage_config(url_in)  # also covers from_url
    assert safe_url == config.to_url()
    assert full_url == config.to_url(params=True)
    assert str(config) == f"FSConfig('{safe_url}')"


deprecated_urls = [
    ("fs:///?user=123", "fs:///", "fs:///?chown=123"),
    ("fs:///?uid=123", "fs:///", "fs:///?chown=123"),
    ("fs:///?uid=123&gid=456", "fs:///", "fs:///?chown=123:456"),
]


@pytest.mark.parametrize("url_in, safe_url, full_url", deprecated_urls)
def test_fs_config_backwards_compat(url_in, safe_url, full_url):
    """Test deprecated but still supported fs config options (query params)."""
    config = create_storage_config(url_in)
    assert safe_url == config.safe_url
    assert full_url == config.full_url


invalid_config_urls = [
    ("fs://?foo=bar", "Extra inputs are not permitted"),
    ("fs://?chmod=foo", "String should match pattern"),
]


@pytest.mark.parametrize("url_in, error", invalid_config_urls)
def test_fs_config_factory_invalid_url(url_in, error):
    """Test errors caught on config validation."""
    with pytest.raises(ValueError, match=error):
        create_storage_config(url_in)


invalid_client_urls = [
    ("fs://foo", "path doesn't exist"),
    ("fs://pyproject.toml", "path is not a dir"),
]


def test_fs_config_override_type_coercion():
    override = FSConfigOverride(chown=1000, chmod=666)
    assert override.chown == "1000"
    assert override.chmod == "666"


@pytest.mark.parametrize("url_in, error", invalid_client_urls)
def test_fs_client_factory_invalid_url(url_in, error):
    """Test errors caught on client initialization."""
    with pytest.raises(errors.StorageError, match=error):
        create_storage_client(url_in)


def test_fs_client_str():
    """Test client stringification w/ and w/o opts (should never show params)."""
    client = create_storage_client("fs:///tmp")
    assert str(client) == "FSStorage('fs:///tmp')"

    client = create_storage_client("fs:///tmp?follow_links=1")
    assert str(client) == "FSStorage('fs:///tmp')"


# path transformation / canonization methods
def test_fs_client_relpath():
    client = create_storage_client("fs:///")
    assert client.relpath() == ""
    assert client.relpath("foo") == "foo"
    assert client.relpath("/foo") == "foo"


def test_fs_client_abspath():
    client = create_storage_client("fs:///")
    assert client.abspath() == "/"
    assert client.abspath("foo") == "/foo"
    assert client.abspath("/foo") == "/foo"


def test_fs_client_urlpath():
    client = create_storage_client("fs:///")
    assert client.urlpath() == "fs:///"
    assert client.urlpath("foo") == "fs:///foo"
    assert client.urlpath("/foo") == "fs:///foo"


def test_fs_client_test_rw(tmp_path):
    """Test client self-test in read and write modes (w/o leftovers)."""
    client = create_storage_client(f"fs://{tmp_path}")
    assert client.test() is None
    assert client.test(mode="r") is None
    assert client.test(mode="w") is None
    files = sorted(str(f) for f in tmp_path.glob("**") if f.is_file())
    assert not files, "test(mode='w') didn't clean up"


def test_fs_client_ls(tmp_path):
    """Test ls output and order at root/path, w/ and w/o filters."""
    client = create_storage_client(f"fs://{tmp_path}")
    # NOTE sort: numbers < uppercase < lowercase
    files = ["a", "A/baz", "1", "2/foo", "2/bar"]
    for file in files:
        path = tmp_path / file
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_text(file)

    ls_root = [f.path for f in client.ls()]
    assert ls_root == ["1", "2/bar", "2/foo", "A/baz", "a"]

    ls_path = [f.path for f in client.ls("2")]
    assert ls_path == ["2/bar", "2/foo"]

    ls_filt = [f.path for f in client.ls(filt=lambda f: "/ba" in f.path)]
    assert ls_filt == ["2/bar", "A/baz"]

    # filt_dir and filt_file is *not* part of the abstract interface
    ls_filt_dir = [f.path for f in client.ls(filt_dir=lambda d: d != "2")]
    assert ls_filt_dir == ["1", "A/baz", "a"]

    ls_filt_file = [f.path for f in client.ls(filt_file=lambda f: f[0] == "b")]
    assert ls_filt_file == ["2/bar", "A/baz"]


def test_fs_client_stat(tmp_path):
    """Test stat, the available fields and the content-hashing opt."""
    client = create_storage_client(f"fs://{tmp_path}")
    path = tmp_path / "dir/name.ext"
    path.parent.mkdir()
    path.write_bytes(b"bytes")

    file = client.stat("dir/name.ext")
    assert file.path == "dir/name.ext"
    assert file.size == 5
    assert file.ctime == path.stat().st_ctime
    assert file.mtime == path.stat().st_mtime
    assert file.hash is not None

    assert file.dir == "dir"  # item @props
    assert file.name == "name.ext"
    assert file.stem == "name"
    assert file.ext == "ext"

    assert file.created == file.ctime  # compat
    assert file.modified == file.mtime


def test_fs_client_open(tmp_path):
    """Test open in write and in read mode, verifying the content written."""
    client = create_storage_client(f"fs://{tmp_path}")
    path = tmp_path / "foo/bar"
    with client.open("foo/bar", "w") as file:
        file.write(b"bytes")
    assert path.read_bytes() == b"bytes"
    with client.open("foo/bar", "r") as file:
        assert file.read() == b"bytes"


def test_fs_client_open_errors(tmp_path, mocker):
    """Test open error handling."""
    client = create_storage_client(f"fs://{tmp_path}")
    with pytest.raises(errors.FileNotFound, match="No such file or directory"):
        client.open("foo")
    path = tmp_path / "foo"
    path.touch()
    with pytest.raises(errors.NotADirectory, match="Not a directory"):
        client.open("foo/bar")  # raises from python core
    with pytest.raises(errors.NotADirectory, match="Not a directory"):
        client.open("foo/bar", "w")  # raises from fs client
    path.chmod(0o000)
    errmsg = "Permission denied"
    if root:
        mocker.patch.object(Path, "open", side_effect=PermissionError(errmsg))
    with pytest.raises(errors.PermissionError, match=errmsg):
        client.open("foo")


def test_fs_client_rm(tmp_path):
    """Test rm to remove files and to require recurse for dirs."""
    client = create_storage_client(f"fs://{tmp_path}")
    path = tmp_path / "foo/bar"
    with client.open("foo/bar", "w") as file:
        file.write(b"bytes")
    assert path.exists()
    client.rm("foo/bar")
    assert not path.exists()
    with pytest.raises(errors.FileNotFound, match="No such file or directory"):
        client.rm("foo/bar")
    with pytest.raises(errors.IsADirectory, match="cannot remove dir w/o recurse=True"):
        client.rm("foo")
    client.rm("foo", recurse=True)
    assert not path.parent.exists()


def test_fs_client_opt_cleanup_dirs_with_context(tmp_path):
    path = tmp_path / "foo"
    path.mkdir()
    with create_storage_client(f"fs://{tmp_path}?cleanup_dirs=1"):
        assert path.is_dir()
    assert not path.exists()


def test_fs_client_opt_content_hash(tmp_path):
    """Test content_hash opt, which should add content md5 in ls and stat results."""
    client = create_storage_client(f"fs://{tmp_path}?content_hash=1")
    path = tmp_path / "foo"
    path.write_bytes(b"bytes")
    bytes_md5 = "4b3a6218bb3e3a7303e8a171a60fcf92"
    assert list(client.ls())[0].hash == bytes_md5
    assert client.stat(path).hash == bytes_md5


@pytest.mark.skip(reason="TODO")
def test_fs_client_opt_follow_links():
    pass


def test_fs_client_opt_groups(tmp_path, mocker):
    """Test groups opt, which should set supplementary groups in the storage ctx."""
    # simply wrap orig call when running as root (pre-commit/ci)
    if root:
        orig_groups = os.getgroups()  # returned [0], now [] in CI
        setgroups = mocker.patch("os.setgroups", wraps=os.setgroups)
    else:
        orig_groups = [0]
        ctx = {"groups": orig_groups.copy()}
        setter = partial(partial, ctx.__setitem__)
        mocker.patch("os.getgroups", side_effect=lambda: ctx["groups"])
        setgroups = mocker.patch("os.setgroups", side_effect=setter("groups"))
    # check that supplementary groups are set when entering the context
    with create_storage_client(f"fs://{tmp_path}?groups=123,456") as client:
        assert setgroups.mock_calls == [call([123, 456])]
        with client.open("foo/bar", "w") as file:
            file.write(b"bytes")
    # check that supplementary groups are restored when exiting the context
    assert setgroups.mock_calls == [call([123, 456]), call(orig_groups)]


def test_fs_client_opt_chown(tmp_path, mocker):
    """Test chown opt, which should set uid/gid within the storage context."""
    # simply wrap orig call when running as root (pre-commit/ci)
    if root:
        os.system(f"chown -R 123:456 {tmp_path.parents[1]}")
        seteuid = mocker.patch("os.seteuid", wraps=os.seteuid)
        setegid = mocker.patch("os.setegid", wraps=os.setegid)
    else:
        ctx = {"uid": 0, "gid": 0}
        setter = partial(partial, ctx.__setitem__)
        mocker.patch("os.geteuid", side_effect=lambda: ctx["uid"])
        mocker.patch("os.getegid", side_effect=lambda: ctx["gid"])
        seteuid = mocker.patch("os.seteuid", side_effect=setter("uid"))
        setegid = mocker.patch("os.setegid", side_effect=setter("gid"))
    # check that uid/gid is set when entering the context
    with create_storage_client(f"fs://{tmp_path}?chown=123:456") as client:
        assert seteuid.mock_calls == [call(123)]
        assert setegid.mock_calls == [call(456)]
        with client.open("foo/bar", "w") as file:
            file.write(b"bytes")
    # check that uid/gid is restored when exiting the context
    assert seteuid.mock_calls == [call(123), call(0)]
    assert setegid.mock_calls == [call(456), call(0)]
    # verify the actual uid/gid when running as root
    if root:
        path = tmp_path / "foo/bar"
        assert path.stat().st_uid == path.parent.stat().st_uid == 123
        assert path.stat().st_gid == path.parent.stat().st_gid == 456
        os.system(f"chown -R 0:0 {tmp_path.parents[1]}")


def test_fs_client_opt_chmod(tmp_path):
    """Test chmod opt, which should apply perms to files and parent dirs."""
    client = create_storage_client(f"fs://{tmp_path}?chmod=640")
    path = tmp_path / "foo/bar"
    with client.open("foo/bar", "w") as file:
        file.write(b"bytes")
    assert stat.S_IMODE(path.stat().st_mode) == 0o640
    assert stat.S_IMODE(path.parent.stat().st_mode) == 0o750
