"""Tests for Azure Cloud storage."""

import io

import pytest
from azure.storage.blob import ContainerClient
from pydantic import ValidationError

from fw_storage import config, create_storage_client
from fw_storage.config import AZConfig
from fw_storage.types.az import AZStorage


@pytest.fixture
def cfg():
    keys = ["LOAD_ENV", "REQUIRE_CREDS"]
    orig = {key: getattr(config, key) for key in keys}
    for key in keys:
        setattr(config, key, False)
    yield config
    for key in keys:
        setattr(config, key, orig[key])


def test_init_with_access_key(mocker):
    create_client_mock = mocker.patch("fw_storage.types.az.create_default_client")
    create_client_mock.return_value = mocker.MagicMock(spec=ContainerClient)
    access_key = (
        "J94I0uS13Cc79AvAq33Hrkt3+C40yq16IF058yQUyiM7"
        "U2qBwGJXQ2VIrLhy0gwGRWMQ2OLTpJ6C9PsEXAMPLE=="
    )
    url = f"az://acct.blob.core.windows.net/container?access_key={access_key}"
    az = create_storage_client(url)

    assert create_client_mock.call_args[0] == (
        "acct.blob.core.windows.net",
        "container",
        access_key,
    )

    assert az.abspath("random/path/without/prefix") == "random/path/without/prefix"


def test_init(mocker):
    create_client_mock = mocker.patch("fw_storage.types.az.create_default_client")
    create_client_mock.return_value = mocker.MagicMock(spec=ContainerClient)
    access_key = (
        "J94I0uS13Cc79AvAq33Hrkt3+C40yq16IF058yQUyiM7"
        "U2qBwGJXQ2VIrLhy0gwGRWMQ2OLTpJ6C9PsEXAMPLE=="
    )
    url = f"az://acct.blob.core.windows.net/container?access_key={access_key}"
    az = AZStorage(AZConfig.from_url(url))

    assert create_client_mock.call_args[0] == (
        "acct.blob.core.windows.net",
        "container",
        access_key,
    )

    assert az.abspath("random/path/without/prefix") == "random/path/without/prefix"


def test_init_with_implicit_creds(cfg, mocker):
    cfg.LOAD_ENV = False
    cfg.REQUIRE_CREDS = True

    create_client_mock = mocker.patch("fw_storage.types.az.create_default_client")
    create_client_mock.return_value = mocker.MagicMock(spec=ContainerClient)

    with pytest.raises(ValidationError):
        create_storage_client("az://acct.blob.core.windows.net/container")

    az = create_storage_client(
        "az://acct.blob.core.windows.net/container?connector_creds=true"
    )
    az.set("foo", io.BytesIO(b"bar"))
    assert az.stat("foo")

    assert (
        az.config.full_url
        == "az://acct.blob.core.windows.net/container?connector_creds=true"
    )


# TODO test errors with invalid creds
