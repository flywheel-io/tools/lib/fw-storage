import datetime as dt
import json
import time
from email.parser import Parser
from email.utils import formatdate

import jwt
import requests


def get_now():
    return dt.datetime.now(dt.timezone.utc).replace(tzinfo=None)


def configure_gs_stub(server):  # noqa
    buckets = {}

    def create_bucket():
        bucket_name = server.request.json["name"]
        buckets[bucket_name] = {"files": {}, "parts": {}}
        return {}, 200

    def get_bucket(bucket):
        if bucket in buckets:
            return {}, 200
        return {}, 404

    def upload(bucket):
        if bucket not in buckets:
            return {}, 404
        content = server.request.get_data()
        content_type = server.request.content_type
        upload_type = server.request.args.get("uploadType")
        if upload_type == "resumable":
            payload = json.loads(content)
            name = payload["name"]
            buckets[bucket]["files"][name] = {"name": name}
            location = f"{server.url}/storage/v1/b/{bucket}/o/{name}"
            return {}, 200, {"Location": location}
        parts = iter_parts(content, content_type)
        timestamp = get_now().isoformat() + "Z"
        for payload, content in zip(parts, parts):
            payload = json.loads(payload)
            name = payload["name"]
            buckets[bucket]["files"][name] = {
                "name": name,
                "content": content,
                "timeCreated": timestamp,
                "updated": timestamp,
                "size": len(content),
            }
        return {}, 200

    def list_blobs(bucket):
        prefix = server.request.args.get("prefix")
        items = []
        for file in sorted(buckets[bucket]["files"].values(), key=lambda f: f["name"]):
            if file["name"].startswith(prefix):
                items.append({k: v for k, v in file.items() if k != "content"})
        return {"items": items}, 200

    def get_blob(bucket, key):
        if key not in buckets[bucket]["files"]:
            return {}, 404
        if server.request.method == "DELETE":
            buckets[bucket]["files"].pop(key)
            return {}, 200
        if server.request.method == "GET":
            file = buckets[bucket]["files"][key]
            return {k: v for k, v in file.items() if k != "content"}, 200
        if server.request.method == "PUT":
            content = server.request.get_data()
            timestamp = get_now().isoformat() + "Z"
            buckets[bucket]["files"][key] = {
                "name": key,
                "content": content,
                "timeCreated": timestamp,
                "updated": timestamp,
                "size": len(content),
            }
            return {}, 200
        return {}, 400

    def batch():
        content = server.request.get_data()
        content_type = server.request.content_type
        req_parts = iter_parts(content, content_type)
        resp_parts = []
        for part in req_parts:
            method, url, *_ = part.split("\n")[0].split()
            resp = requests.request(method, url, timeout=0.1)
            resp_parts.append(
                "Content-Type: application/http\r\n\r\n"
                "HTTP/1.1 200 OK\r\n"
                "Content-Type: application/json\r\n\r\n"
                f"{resp.text}",
            )
        boundary = "batch1234"
        headers = {"content-type": f"multipart/mixed; boundary={boundary}"}
        body = f"\r\n\r\n--{boundary}\r\n".join(["", *resp_parts])
        return body, 200, headers

    def download(bucket, key):
        if key not in buckets[bucket]["files"]:
            return {}, 404
        return buckets[bucket]["files"][key]["content"], 200

    def multipart_uploads(bucket, key):
        xmlns = "http://s3.amazonaws.com/doc/2006-03-01/"
        if "uploads" in server.request.args:
            u_id = "VXBsb2FkIElEIGZvciBlbHZpbmcncyBteS1tb3ZpZS5tMnRzIHVwbG9hZA"
            response = f"""
            <InitiateMultipartUploadResult xmlns="{xmlns}">
                <Bucket>{bucket}</Bucket>
                <Key>{key}</Key>
                <UploadId>{u_id}</UploadId>
            </InitiateMultipartUploadResult>
            """
        if "uploadId" in server.request.args:
            response = f"""
            <CompleteMultipartUploadResult xmlns="{xmlns}">
                <Location>http://{bucket}.storage.googleapis.com/{key}</Location>
                <Bucket>{bucket}</Bucket>
                <Key>{key}</Key>
                <ETag>"7fc8f92280ac3c975f300cb64412c16f-9"</ETag>
            </CompleteMultipartUploadResult>
            """
        return response, 200

    def multipart_upload_part(bucket, key):
        # basic multipart upload stub only works with one part
        content = server.request.get_data()
        timestamp = get_now().isoformat() + "Z"
        buckets[bucket]["files"][key] = {
            "name": key,
            "content": content,
            "timeCreated": timestamp,
            "updated": timestamp,
            "size": len(content),
        }
        return b"", 200

    token = {
        "access_token": "access_token",
        "expires_in": time.time() + 3600,
        "id_token": "",
        "scope": "",
        "token_type": "Bearer",
        "refresh_token": "refresh",
    }
    response = server.add_response
    callback = server.add_callback
    response("/token", token, "POST")
    callback("/storage/v1/b", create_bucket, ["POST"])
    callback("/storage/v1/b/<bucket>", get_bucket, ["GET"])
    callback("/upload/storage/v1/b/<bucket>/o", upload, ["POST"])
    callback("/storage/v1/b/<bucket>/o", list_blobs, ["GET"])
    callback("/storage/v1/b/<bucket>/o/<path:key>", get_blob, ["GET", "PUT", "DELETE"])
    callback("/download/storage/v1/b/<bucket>/o/<path:key>", download, ["GET"])
    callback("/batch/storage/v1", batch, ["POST"])
    callback("/<bucket>/<path:key>", download, ["GET"])
    callback("/<bucket>/<path:key>", multipart_upload_part, ["PUT"])
    callback("/<bucket>/<path:key>", multipart_uploads, ["POST"])
    return server


def configure_az_stub(server):  # noqa
    containers = {}

    def create_container(**kwargs):
        containers[kwargs["container"]] = {"files": {}}
        return "", 201

    def get_container(**kwargs):
        if server.request.args.get("comp") == "list":
            return list_blobs(kwargs["container"])
        if kwargs["container"] in containers:
            return "", 200
        return "", 404

    def list_blobs(container):
        prefix = server.request.args.get("prefix")
        blobs = ""
        for file in sorted(
            containers[container]["files"].values(), key=lambda f: f["name"]
        ):
            if not file["name"].startswith(prefix):
                continue
            created = file["creation_time"]
            updated = file["last_modified"]
            blobs += f"""<Blob>
              <Name>{file["name"]}</Name>
              <Properties>
                <Creation-Time>{created}</Creation-Time>
                <Last-Modified>{updated}</Last-Modified>
                <Etag>637508792730222500</Etag>
                <Content-Length>{file["size"]}</Content-Length>
                <Content-Type>application/octet-stream</Content-Type>
                <Content-Encoding></Content-Encoding>
                <Content-Language></Content-Language>
                <Content-CRC64></Content-CRC64>
                <Content-MD5>gtDw+oVR3ot+tey2Xq4CYQ==</Content-MD5>
                <Cache-Control></Cache-Control>
                <Content-Disposition></Content-Disposition>
                <BlobType>BlockBlob</BlobType>
                <AccessTier>Hot</AccessTier>
                <AccessTierInferred>true</AccessTierInferred>
                <LeaseStatus>unlocked</LeaseStatus>
                <LeaseState>available</LeaseState>
                <ServerEncrypted>true</ServerEncrypted>
              </Properties>
              <OrMetadata></OrMetadata>
            </Blob>
            """

        blob_items = f"""<?xml version="1.0" encoding="UTF-8" ?>
        <EnumerationResults>
          <Blobs>
            {blobs}
          </Blobs>
          <NextMarker></NextMarker>
        </EnumerationResults>
        """
        headers = {"content-type": "application/xml"}
        return blob_items, 200, headers

    def get_blob(**kwargs):
        container = kwargs["container"]
        blob = kwargs["blob"]
        if server.request.method == "DELETE":
            containers[container]["files"].pop(blob)
            return "", 202

        if server.request.method == "PUT":
            # NOTE it is a very dump block upload implementation
            # not working with multiple blocks
            if server.request.args.get("comp") == "blocklist":
                return "", 201
            content = server.request.get_data()
            timestamp = formatdate(
                timeval=time.mktime(get_now().timetuple()),
                localtime=False,
                usegmt=True,
            )
            containers[container]["files"][blob] = {
                "name": blob,
                "content": content,
                "creation_time": timestamp,
                "last_modified": timestamp,
                "size": len(content),
            }
            return "", 201

        # GET, HEAD as default
        if blob not in containers[container]["files"]:
            return "", 404
        file = containers[container]["files"][blob]
        headers = {
            # "Content-Length": file["size"],
            "Content-Range": f"bytes 0-{len(file['content'])}/{len(file['content'])}",
            "Content-Type": "application/octet-stream",
            "Content-MD5": "gtDw+oVR3ot+tey2Xq4CYQ==",
            "Last-Modified": file["last_modified"],
            "Accept-Ranges": "bytes",
            "ETag": "'0x8D8E30B7787AAEE'",
            "Server": "Windows-Azure-Blob/1.0 Microsoft-HTTPAPI/2.0",
            "x-ms-request-id": "4a3cef3f-601e-006d-73f4-14df28000100",
            "x-ms-client-request-id": "9f4f1ad8-80e7-11eb-b6be-a0a4c581b83a",
            "x-ms-version": "2020-04-08",
            "x-ms-creation-time": file["creation_time"],
            "x-ms-lease-status": "unlocked",
            "x-ms-lease-state": "available",
            "x-ms-blob-type": "BlockBlob",
            "x-ms-server-encrypted": "true",
            "x-ms-access-tier": "Hot",
            "x-ms-access-tier-inferred": "true",
            "Date": "Tue, 09 Mar 2021 14:56:26 GMT",
        }
        return file["content"], 200, headers

    def token():
        jwt_token = jwt.encode(
            {
                "iss": f"{server.url}/oauth2/default",
                "aud": "wilson",
                "iat": 1604216538,
                "exp": time.time() + 3600,
                "sub": "sub",
            },
            "secret",
        )

        return {
            "access_token": "access_token",
            "expires_in": time.time() + 3600,
            "id_token": jwt_token,
            "scope": server.url,
            "token_type": "Bearer",
            "refresh_token": "refresh",
        }, 200

    def get_openid(**_):
        return {
            "token_endpoint": f"{server.url}/token",
            "authorization_endpoint": f"{server.url}/auth",
        }

    def post_batch(**_):
        content = server.request.get_data()
        content_type = server.request.content_type
        req_parts = iter_parts(content, content_type)
        resp_parts = []
        for part in req_parts:
            method, path, *_ = part.split("\n")[0].split()
            url = f"{server.url}{path}"
            resp = requests.request(method, url, timeout=0.1)
            resp_parts.append(
                "Content-Type: application/http\r\n\r\n"
                "HTTP/1.1 200 OK\r\n"
                "Content-Type: application/json\r\n\r\n"
                f"{resp.text}",
            )
        boundary = "batch1234"
        headers = {"content-type": f"multipart/mixed; boundary={boundary}"}
        body = f"\r\n\r\n--{boundary}\r\n".join(["", *resp_parts])
        return body, 202, headers

    disc_resp = {"tenant_discovery_endpoint": "/tenant-discovery", "metadata": {}}

    response, callback = server.add_response, server.add_callback

    response("/login.microsoftonline.com/common/discovery/instance", disc_resp, "GET")
    response("/auth", token, "GET")
    callback("/token", token, ["POST"])
    callback("/<t_name>/v2.0/.well-known/openid-configuration", get_openid, ["GET"])
    callback("/<container>", create_container, ["PUT"])
    callback("/<container>", get_container, ["GET"])
    callback("/<container>", post_batch, ["POST"])
    callback("/<container>/<path:blob>", get_blob, ["GET", "PUT", "DELETE"])
    callback("/storage/v1/b/<bucket>/o", list_blobs, ["GET"])
    return server


def iter_parts(body, content_type):
    parser = Parser()
    message = f"Content-Type: {content_type}\n\n{body.decode()}"
    request = parser.parsestr(message)
    if not request.is_multipart():
        raise ValueError("Expected multi-part")
    for sub in request.get_payload():
        yield sub.get_payload()
