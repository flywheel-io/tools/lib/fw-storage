"""Tests for local storage."""

import os
from pathlib import Path

import pytest

from fw_storage import IsADirectory, PermError, create_storage_client
from fw_storage.config import FSConfig
from fw_storage.types.fs import FSStorage

from .conftest import create_files


@pytest.fixture
def fs_url(request, prepare_url):
    url = request.param
    if not url.startswith("fs"):
        raise ValueError(f"Expected fs url, got: {url}")
    return prepare_url(url)


@pytest.fixture
def fs(fs_url):
    return create_storage_client(fs_url)


@pytest.fixture
def chdir(tmp_path):
    cwd = os.getcwd()
    os.chdir(tmp_path)
    yield tmp_path
    os.chdir(cwd)


def test_factory(tmp_path):
    fs = create_storage_client(f"fs://{tmp_path}")
    assert isinstance(fs, FSStorage)
    # TODO standardize strs/reprs
    # assert repr(fs) == f"FSStorage('{tmp_path}')"


def test_init(tmp_path):
    fs = FSStorage(FSConfig.from_url(f"fs://{tmp_path}"))
    assert isinstance(fs, FSStorage)


# TODO test error if root dir doesn't exist (and read-only?)


@pytest.mark.skipif(os.getuid() == 0, reason="non-root required")
def test_write_permission_error(fs):
    Path(fs.config.path).chmod(0o000)
    with pytest.raises(PermError):
        fs.test_write()
    with pytest.raises(PermError):
        fs.set("foo", b"bar")
    # does not raise if permissions are ok
    Path(fs.config.path).chmod(0o777)
    fs.test_write()
    fs.set("foo", b"bar")


def test_rm_raises_deleting_dir_wo_recurse(fs):
    create_files(fs, "foo/bar")
    with pytest.raises(IsADirectory):
        fs.rm("foo")


def test_rm_empty_dirs(fs):
    create_files(fs, "foo/bar.txt")
    fs.rm("foo/bar.txt")
    fs.rm_empty_dirs()
    assert fs.prefix.exists()
    assert not (fs.prefix / "foo").exists()

    create_files(fs, "foo/bar/foo.txt", "foo/deadbeef/bar.txt", "lol/foo.txt")
    fs.rm("foo/deadbeef/bar.txt")
    fs.rm("lol/foo.txt")
    fs.rm_empty_dirs()
    assert fs.prefix.exists()
    assert not (fs.prefix / "foo/deadbeef").exists()
    assert not (fs.prefix / "lol").exists()
    assert (fs.prefix / "foo/bar").exists()
    assert (fs.prefix / "foo").exists()


@pytest.mark.skip(reason="fs dir filtering changed and isn't officially exposed")
def test_ls_dir_filter(fs):
    create_files(fs, "foo/a.txt", "bar/c.txt")

    include = ["dir=~foo"]
    assert [f.path for f in fs.ls(include=include)] == ["foo/a.txt"]

    exclude = ["dir=~bar"]
    assert [f.path for f in fs.ls(exclude=exclude)] == ["foo/a.txt"]

    assert [f.path for f in fs.ls(include=include, exclude=exclude)] == ["foo/a.txt"]


@pytest.mark.skipif(os.geteuid() != 0, reason="root required")
def test_set_with_gid_and_uid_as_root(tmp_path):
    os.system(f"chown -R 123:456 {tmp_path.parents[1]}")
    with create_storage_client(f"fs://{tmp_path}?user=123:456") as fs:
        fs.set("parent/test1.txt", b"test")
    stat = os.stat(f"{tmp_path}/parent/test1.txt")
    assert stat.st_uid == 123
    assert stat.st_gid == 456
    parent_stat = os.stat(f"{tmp_path}/parent")
    assert parent_stat.st_uid == 123
    assert parent_stat.st_gid == 456

    with create_storage_client(f"fs://{tmp_path}?user=123") as fs:
        fs.set("test2.txt", b"test")
    stat = os.stat(f"{tmp_path}/test2.txt")
    assert stat.st_uid == 123

    with create_storage_client(f"fs://{tmp_path}?uid=123") as fs:
        fs.set("test3.txt", b"test")
    stat = os.stat(f"{tmp_path}/test3.txt")
    assert stat.st_uid == 123
    os.system(f"chown -R 0:0 {tmp_path.parents[1]}")


def test_windows_style_path(chdir):
    (chdir / "c:/path").mkdir(parents=True)
    with create_storage_client("fs://c:/path") as fs:
        assert fs.config.path.endswith("c:/path")
        fs.set("test.txt", b"test")
        assert (chdir / "c:/path/test.txt").exists()
