"""Pytest conftest module."""

import functools
import json
import os
import re
import typing as t
from importlib import import_module
from urllib.parse import urlparse
from uuid import uuid4

import pytest
import requests
from azure.storage.blob import BlobServiceClient
from fw_utils import parse_url
from google.oauth2.service_account import Credentials

from fw_storage import Storage, create_storage_client
from fw_storage.storage import STORAGE_CLASSES
from fw_storage.types import az

from .helpers import configure_az_stub, configure_gs_stub

TEST_STORAGE_CLASSES = set(STORAGE_CLASSES)

pytest_plugins = "fw_http_testserver"

File = t.Union[str, t.Tuple[str, t.Union[str, bytes]]]
az.create_default_client = functools.cache(az.create_default_client)


def create_files(storage: Storage, *files: File) -> None:
    """Create files in the given storage for testing."""
    for file in files or []:
        if isinstance(file, tuple):
            file, data = file
        else:
            data = file
        if isinstance(data, str):
            data = data.encode()
        storage.set(file, data)


def pytest_generate_tests(metafunc):
    # load any explicitly configured storage urls from env
    env_urls = set(os.environ.get("TEST_STORAGE_URLS", "").split())
    # load the default storage urls only containing the scheme (contract tests)
    stub_urls = {f"{scheme}://" for scheme in TEST_STORAGE_CLASSES}
    # tested urls are a union of the ones configured and the stubs
    urls = sorted(env_urls.union(stub_urls))
    if os.environ.get("CI_JOB_ID"):
        # require real storage backends in CI to enforce integrated testing
        required = set(TEST_STORAGE_CLASSES) - {"fs"}  # fs is always real
        configured = {parse_url(url)["scheme"] for url in env_urls - stub_urls}
        missing = sorted(required - configured)
        if missing:
            raise ValueError(f"Missing required storage backend URLs: {missing}")
    # parametrize common storage url fixture
    if "storage_url" in metafunc.fixturenames:
        # for common / generic storage interface tests in `test_storage.py`
        metafunc.parametrize("storage_url", urls, indirect=True)
    # parametrize storage-specific url fixtures
    for storage_type in TEST_STORAGE_CLASSES:
        # for storage-backend-specific tests in `test_<scheme>.py`
        fixture_name = f"{storage_type}_url"
        if fixture_name in metafunc.fixturenames:
            urls = [url for url in urls if url.startswith(storage_type)]
            metafunc.parametrize(fixture_name, urls, indirect=True)


@pytest.fixture(autouse=True)
def env(mocker):
    env = {"AZURE_RETRY_BACKOFF": "0"}
    mocker.patch.dict(os.environ, env)


@pytest.fixture
def prepare_url(tmp_path, az_mock, gs_mock, s3_mock):
    """Prepare and track storage urls, finally cleanup resources.

    This fixture allows us to use real/mock storage backends. It starts
    automatically the mock if the url only contains the scheme. For local
    storage we use tmp path in this case which is a real filesystem. Also
    add a uuid4 suffix to the url which helps to avoid interference between
    tests and makes possible to cleanup any resources created by tests.
    """
    urls = set()

    def prepare_url_(url):
        suffix = str(uuid4())
        if url == "fs://":
            (tmp_path / suffix).mkdir()
            url = f"fs://{tmp_path}/{suffix}"
        elif url == "gs://":
            gs_mock()
            url = f"gs://fw-storage/{suffix}"
        elif url == "s3://":
            s3_mock()
            url = f"s3://fw-storage/{suffix}"
            url += "?access_key_id=valid_access_key_id"
            url += "&secret_access_key=valid_secret_with_enough_characters_1234"
        elif url == "az://":
            az_mock()
            url = f"az://acct.blob.core.windows.net/fw-storage/{suffix}?access_key="
            url += "J94I0uS13Cc79AvAq33Hrkt3+C40yq16IF058yQUyiM7"
            url += "U2qBwGJXQ2VIrLhy0gwGRWMQ2OLTpJ6C9PsEXAMPLE=="
        urls.add(url)
        return url

    yield prepare_url_

    for url_ in urls:
        storage = create_storage_client(url_)
        for fileinfo in storage.ls():
            storage.rm(fileinfo)
        storage.cleanup()


@pytest.fixture
def az_mock(http_testserver, mocker, monkeypatch):
    def az_mock_(container="fw-storage"):
        patch, s_addr, s_url = mocker.patch, http_testserver.addr, http_testserver.url
        monkeypatch.setenv("AZURE_CLIENT_ID", "wilson")
        monkeypatch.setenv("AZURE_TENANT_ID", "id")
        monkeypatch.setenv("AZURE_CLIENT_SECRET", "castaway")
        monkeypatch.setenv("AZURE_RETRY_TOTAL", "0")

        def tweak_url(func):
            def wrapped(self, method, url, **kwargs):
                url = re.sub(r"https://[^/]+(/.*)", rf"{s_url}\1", url)
                return func(self, method, url, **kwargs)

            return wrapped

        def canon(url):
            auth = urlparse(url)
            parts = auth.path.split("/")
            return auth, auth.hostname, parts[1]

        # do the patches
        patch("requests.Session.request", tweak_url(requests.Session.request))

        def noop(_):
            return _

        az_id = "azure.identity"
        creds_base = "_internal.client_credential_base.ClientCredentialBase"
        patch(f"{az_id}._internal.normalize_authority", noop)
        patch(f"{az_id}._internal.msal_credentials.normalize_authority", noop)
        patch(f"{az_id}._constants.KnownAuthorities.AZURE_PUBLIC_CLOUD", s_url)
        patch(f"{az_id}.{creds_base}._acquire_token_silently", return_value=None)
        patch("msal.authority.canonicalize", canon)
        patch("msal.token_cache.canonicalize", canon)
        patch("msal.authority.WELL_KNOWN_AUTHORITY_HOSTS", [http_testserver.host])

        configure_az_stub(http_testserver)
        # create default container
        if container:
            client = BlobServiceClient(account_url=s_addr, retry_total=0)
            client.get_container_client(container).create_container()
        return http_testserver

    return az_mock_


@pytest.fixture
def gs_mock(http_testserver, tmp_path, mocker, monkeypatch):
    def gs_mock_(bucket="fw-storage"):
        patch = mocker.patch
        key_file = tmp_path / "key.json"
        key_file.write_text(json.dumps(GS_SVC_KEY))
        monkeypatch.setenv("GOOGLE_CLOUD_PROJECT", "project")
        monkeypatch.setenv("GOOGLE_APPLICATION_CREDENTIALS", str(key_file))
        api_endpoint = "google.cloud.storage._http.Connection.DEFAULT_API_ENDPOINT"
        token_endpoint = "google.oauth2.credentials._GOOGLE_OAUTH2_TOKEN_ENDPOINT"  # noqa
        patch(api_endpoint, http_testserver.url)
        patch(token_endpoint, f"{http_testserver.url}/token")
        patch_google_creds(mocker)
        configure_gs_stub(http_testserver)
        # create default bucket
        if bucket:
            gs = import_module("google.cloud.storage")
            gs.Client().create_bucket(bucket)
        return http_testserver

    return gs_mock_


@pytest.fixture
def s3_mock(monkeypatch):
    moto = import_module("moto")
    mock = moto.mock_aws()

    def s3_mock_(bucket="fw-storage"):
        monkeypatch.setenv("AWS_ACCESS_KEY_ID", "testing")
        monkeypatch.setenv("AWS_SECRET_ACCESS_KEY", "testing")
        if mock._mocks_active:
            mock.stop()
        mock.start(reset=True)

        if bucket:
            boto3 = import_module("boto3")
            client = boto3.client("s3", region_name="us-east-1")
            bucket = "fw-storage"
            client.create_bucket(Bucket=bucket)

    yield s3_mock_
    if mock._mocks_active:
        mock.stop()


def patch_google_creds(mocker):
    def do_nothing(*args, **kwargs):
        pass

    mocker.patch.object(Credentials, "expired", False)
    mocker.patch.object(Credentials, "valid", True)
    mocker.patch.object(Credentials, "refresh", do_nothing)
    mocker.patch.object(Credentials, "apply", do_nothing)
    mocker.patch.object(Credentials, "before_request", do_nothing)
    mocker.patch.object(Credentials, "_create_self_signed_jwt", return_value="")


GS_SVC_KEY = {
    "type": "service_account",
    "project_id": "test",
    "private_key_id": "123456abcd123456abcd123456abcd123456abcd",
    "private_key": "-----BEGIN RSA PRIVATE KEY-----\nMIICXAIBAAKBgQCKVHpMQxbT9BvHNyBPJGCzlTI9bk1pEY8hAVlcyflSfZcTDyQL\nbUUePne46YlpAZhOJPnzkO6fjfyan9BpPo54EYvYfFhGaUvW+DCZnoSicxTNO3EQ\nQt4UBn8xgWWwqERPty/09xHikRayYS5K15IDxH4PhL4lvks4zj8D6sK1AQIDAQAB\nAoGAEYpAr6baICoV057j0+Uy9dJ3Ol7gJfB6C59WK3PYz2LpYxtKYMCMQRd+qs3C\nBLSVEWOS5hV8jHK+kwSssouiGNrDKWkBPTmwNW8SK43h+DrO4SHtjguipYgYGQwi\nCoQ457htFb8wvNR91NCgkPaql2V6H86uaytGcjmT8cI+Y9kCQQDlxvYnXeU2SsXz\nyeXT9g5DdquUFHf2VtJPAp1N78UoRgSQv6r22qk9BhU6rchEe1E6oNFpF2sDNprJ\nsL79C39PAkEAmh3Y5mzZQOJpFWsegKL4GPjD6o9NhrsZNumo1YeVLDfXZp0zAsnc\ncMGGVvLQWms6VJWMNTapLj8FpjjFghLyrwJBAJ6uX1HicyeiTcBFXtWZaFJIwscZ\nt5rEbKqpyI6JFiPw7rgz1VJywUtmRwTl/Jbmfrs5UkT8Kifm7tB4ofzIktsCQF1Q\n6T5MdheFf99KJwP4qYRlTCtAbc7Ahvnq7SGtoLNPdItbb8GmfGWBqFgJWgMFniIN\nEwI9gJ7R5hfJlD24P10CQH5RbTsQoo+LFTAm5q9B9XTGIarVzueSz+Bm+iFGv+/D\n3GcfvgyBPcJNcg9qdnYNpPIHtV0/tp2PjZgzOHkNksw=\n-----END RSA PRIVATE KEY-----",  # gitleaks:allow
    "client_email": "signed-urls-123@test.iam.gserviceaccount.com",
    "client_id": "123456789123456789123",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/signed-urls-123%40test.iam.gserviceaccount.com",
}
