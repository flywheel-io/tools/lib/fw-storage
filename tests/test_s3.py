"""Tests for S3 storage."""

import io

import boto3
import pytest
from botocore.stub import Stubber
from pydantic import ValidationError

from fw_storage import PermError, StorageError, create_storage_client
from fw_storage.config import S3Config
from fw_storage.types.s3 import S3Storage

from .conftest import create_files


@pytest.fixture
def s3_url(request, prepare_url):
    url = request.param
    if not url.startswith("s3"):
        raise ValueError(f"Expected s3 url, got: {url}")
    return prepare_url(url)


@pytest.fixture
def s3(s3_url):
    return create_storage_client(s3_url)


@pytest.fixture
def s3_config():
    return {
        "bucket": "foo",
        "prefix": "pfx",
        "access_key_id": "valid_access_key_id",
        "secret_access_key": "valid_secret_with_enough_characters_1234",
    }


@pytest.fixture
def s3_stub(s3_config):
    """S3 stubber to make testing error cases easier."""
    s3 = S3Storage(S3Config(**s3_config))
    client = boto3.client("s3")
    s3.client = client
    stubber = Stubber(s3.client)
    stubber.activate()
    yield s3, stubber
    stubber.deactivate()


def test_init_with_access_key(mocker):
    client_mock = mocker.patch("fw_storage.types.s3.boto3.session.Session")
    create_storage_client(
        "s3://foo?access_key_id=valid_access_key_id&secret_access_key=valid_secret_with_enough_characters_1234"
    )
    call_kwargs = client_mock.call_args[1]
    assert call_kwargs["aws_access_key_id"] == "valid_access_key_id"
    assert (
        call_kwargs["aws_secret_access_key"]
        == "valid_secret_with_enough_characters_1234"  # noqa
    )


def test_rm_works_in_batches(s3, mocker):
    mocker.patch("fw_storage.types.s3.S3Storage.delete_batch_size", 2)
    create_files(s3, "foo", "bar")
    s3.rm("foo")
    assert len(s3.delete_keys) == 1
    # the file still exists
    assert len(list(s3.ls())) == 2
    # deleting second item triggers flushing pending deletes
    s3.rm("bar")
    assert len(s3.delete_keys) == 0
    assert len(list(s3.ls())) == 0


def test_write_perm(s3_stub):
    s3, stub = s3_stub
    stub.add_client_error("put_object", http_status_code=403)
    with pytest.raises(PermError):
        s3.test_write()


def test_set_set_raise_if_not_writeable(s3_stub):
    s3, stub = s3_stub
    stub.add_client_error("put_object", http_status_code=403)

    with pytest.raises(PermError):
        s3.set("foo", io.BytesIO(b"foo"))


def test_rm_raises_on_errors(s3_stub, mocker):
    mocker.patch("fw_storage.types.s3.S3Storage.delete_batch_size", 1)
    s3, stub = s3_stub
    stub.add_response(
        "delete_objects", {"Errors": [{"Key": "foo", "Message": "Access Denied"}]}
    )

    with pytest.raises(StorageError):
        s3.rm("foo")


def test_raise_on_unknown_error(s3_stub):
    s3, stub = s3_stub
    stub.add_client_error("list_objects_v2", http_status_code=500)
    with pytest.raises(StorageError):
        s3.test_read()


def test_storage_with_implicit_creds(s3_mock):
    s3_mock()
    with pytest.raises(ValidationError):
        create_storage_client("s3://fw-storage")
    s3 = create_storage_client("s3://fw-storage?connector_creds=true")
    s3.set("foo", io.BytesIO(b"foo"))
    assert s3.stat("foo")
    assert s3.config.full_url == "s3://fw-storage?connector_creds=true"
