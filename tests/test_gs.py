"""Tests for Google Cloud storage."""

import json
import os
from base64 import urlsafe_b64encode

import pytest

from fw_storage import PermError, create_storage_client
from fw_storage.config import GSConfig
from fw_storage.types.gs import GSStorage

from .conftest import GS_SVC_KEY, create_files


@pytest.fixture
def gs_url(request, prepare_url):
    url = request.param
    assert url.startswith("gs"), f"Expected gs url, got {url}"
    return prepare_url(url)


@pytest.fixture
def gs(gs_url):
    return create_storage_client(gs_url)


# TODO test error messages


def test_init_creds(mocker, tmp_path):
    env = mocker.patch.dict(os.environ, {}, clear=True)
    key_file = tmp_path / "key.json"
    key_file.write_text(json.dumps(GS_SVC_KEY))

    def assert_creds_from_key(gs):
        svc_email = gs.client._credentials._service_account_email
        assert svc_email == GS_SVC_KEY["client_email"]

    # arg: key path
    gs = create_storage_client(f"gs://foo?service_account_key={key_file}")
    assert gs
    assert_creds_from_key(gs)

    # arg: key contents
    gs = create_storage_client(f"gs://foo?service_account_key={json.dumps(GS_SVC_KEY)}")
    assert gs
    assert_creds_from_key(gs)

    # arg: key contents base64 encoded
    b64key = urlsafe_b64encode(json.dumps(GS_SVC_KEY).encode()).decode()
    gs = create_storage_client(f"gs://foo?service_account_key={b64key}")
    assert gs
    assert_creds_from_key(gs)

    # env: key path
    env["GOOGLE_APPLICATION_CREDENTIALS"] = str(key_file)
    assert create_storage_client("gs://foo")
    assert gs
    assert_creds_from_key(gs)

    # env: key contents
    env["GOOGLE_APPLICATION_CREDENTIALS"] = json.dumps(GS_SVC_KEY)
    gs = create_storage_client("gs://foo")
    assert gs
    assert_creds_from_key(gs)

    gs = GSStorage(GSConfig.from_url(f"gs://foo?service_account_key={key_file}"))
    assert gs
    assert_creds_from_key(gs)


def test_write_perm(gs_mock):
    gs_api = gs_mock()

    # TODO test 'bucket doesn't exist'
    # NOTE right now gs_api returns 500 (KeyError foo), google retries a ton
    # and ultimately raises google.api_core.exceptions.RetryError
    # (which is not handled by the error translation somehow...)
    # create_storage_client("gs://foo")

    # existing bucket w/ write perms OK
    create_storage_client("gs://fw-storage").test_write()

    # existing bucket w/o write perms (server returns 403)
    url = "/upload/storage/v1/b/<bucket>/o"
    gs_api.add_callback(url, lambda bucket: ({}, 403), ["POST"])
    with pytest.raises(PermError):
        create_storage_client("gs://fw-storage").test_write()


def test_rm_batch(gs):
    create_files(gs, "a", "b", "c")
    gs.delete_batch_size = 2
    gs.rm("/", recurse=True)
    # first two (ie. batch) is already deleted, one left in the buffer
    assert len(gs.delete_keys) == 1
    assert len(list(gs.ls())) == 1
    # cleanup calls flush_delete
    gs.cleanup()
    assert not gs.delete_keys
    assert not list(gs.ls())
