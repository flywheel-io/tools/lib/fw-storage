"""Tests for storage config URL (de-)serialization."""

import json
from pathlib import Path

import pytest
from fw_utils import TempEnv

from fw_storage import config


@pytest.fixture
def cfg():
    keys = ["LOAD_ENV", "REQUIRE_CREDS"]
    orig = {key: getattr(config, key) for key in keys}
    for key in keys:
        setattr(config, key, False)
    yield config
    for key in keys:
        setattr(config, key, orig[key])


@pytest.fixture
def env():
    # TODO this should totally get into an fw-utils pytest fixture
    with TempEnv(clear=True) as env:
        yield env


def helpers(cls):
    dict_kw = {
        "secret": "val",
        "exclude": {"type"},
        "exclude_defaults": True,
        "exclude_none": True,
        "exclude_unset": True,
    }
    return (
        lambda url: cls.from_url(url).model_dump(**dict_kw),
        lambda url: cls.from_url(url).safe_url,
        lambda url: cls.from_url(url).full_url,
    )


def test_fs_config():
    data, safe, full = helpers(config.FSConfig)

    cwd = str(Path.cwd())
    assert data("fs://") == {"path": cwd}
    assert safe("fs://") == full("fs://") == f"fs://{cwd}"
    assert data("fs://.") == {"path": cwd}
    assert safe("fs://.") == full("fs://.") == f"fs://{cwd}"

    assert data("fs://rel") == {"path": f"{cwd}/rel"}
    assert safe("fs://rel") == full("fs://rel") == f"fs://{cwd}/rel"
    assert data("fs://rel/dir") == {"path": f"{cwd}/rel/dir"}
    assert safe("fs://rel/dir") == full("fs://rel/dir") == f"fs://{cwd}/rel/dir"

    home = str(Path.home())
    assert data("fs://~") == {"path": home}
    assert safe("fs://~") == full("fs://~") == f"fs://{home}"
    assert data("fs://~/dir") == {"path": f"{home}/dir"}
    assert safe("fs://~/dir") == full("fs://~/dir") == f"fs://{home}/dir"

    assert data("fs:///abs") == {"path": "/abs"}
    assert safe("fs:///abs") == full("fs:///abs") == "fs:///abs"
    assert data("fs:///abs/dir") == {"path": "/abs/dir"}
    assert safe("fs:///abs/dir") == full("fs:///abs/dir") == "fs:///abs/dir"

    assert data("fs:///path?user=1") == {"path": "/path", "chown": "1"}
    assert safe("fs:///path?user=1") == "fs:///path"
    assert full("fs:///path?user=1") == "fs:///path?chown=1"

    with pytest.raises(ValueError, match="Input should be 'fs'"):
        config.FSConfig.from_url("foo://")
    with pytest.raises(ValueError, match="Extra inputs are not permitted "):
        config.FSConfig.from_url("fs:///abs?foo=bar")
    with pytest.raises(DeprecationWarning, match=".*dict.*is deprecated"):
        config.FSConfig.from_url("fs:///").dict()


def test_fs_config_override():
    conf = config.FSConfig.from_url("fs:///path?user=1&groups=1,2")
    assert conf.user == "1"
    assert conf.groups == [1, 2]

    conf.apply_override(config.FSConfigOverride())
    assert conf.user == "1"

    conf.apply_override(config.FSConfigOverride(user="2", groups=[2, 3]))
    assert conf.user == "2"
    assert conf.groups == [2, 3]


def test_s3_config(cfg, env):
    data, safe, full = helpers(config.S3Config)
    access_key_id = "AKIAIOSFODNN7EXAMPLE"  # gitleaks:allow
    secret_access_key = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"  # gitleaks:allow
    creds = {"access_key_id": access_key_id, "secret_access_key": secret_access_key}

    assert data("s3://bkt") == {"bucket": "bkt"}
    assert safe("s3://bkt") == full("s3://bkt") == "s3://bkt"
    assert data("s3://bkt/pfx") == {"bucket": "bkt", "prefix": "pfx"}
    assert safe("s3://bkt/pfx") == full("s3://bkt/pfx/") == "s3://bkt/pfx"

    cfg.REQUIRE_CREDS = True
    with pytest.raises(ValueError, match="access_key_id required"):
        data("s3://bkt")
    query = f"access_key_id={access_key_id}&secret_access_key={secret_access_key}"
    assert data(f"s3://bkt?{query}") == {"bucket": "bkt", **creds}
    assert safe(f"s3://bkt?{query}") == "s3://bkt"
    assert full(f"s3://bkt?{query}") == f"s3://bkt?{query}"

    cfg.LOAD_ENV = True
    with pytest.raises(ValueError, match="access_key_id required"):
        data("s3://bkt")
    env["AWS_ACCESS_KEY_ID"] = access_key_id
    env["AWS_SECRET_ACCESS_KEY"] = secret_access_key
    assert data("s3://bkt") == {"bucket": "bkt", **creds}

    with pytest.raises(ValueError, match="Input should be 's3'"):
        data("foo://")
    with pytest.raises(ValueError, match="at least 3 characters"):
        data("s3://")
    with pytest.raises(AssertionError, match="unexpected foo in url"):
        data("s3://bkt?foo=bar")
    with pytest.raises(DeprecationWarning, match=".*dict.*is deprecated"):
        config.S3Config.from_url("s3://bkt").dict()


def test_s3_config_override(cfg, env):
    conf = config.S3Config.from_url("s3://bkt/pfx")
    assert conf.prefix == "pfx"

    conf.apply_override(config.S3ConfigOverride())
    assert conf.prefix == "pfx"

    conf.apply_override(config.S3ConfigOverride(prefix="abc"))
    assert conf.prefix == "abc"


def test_gs_config(cfg, env, tmp_path):
    data, safe, full = helpers(config.GSConfig)
    creds_json = json.dumps(config.GOOGLE_SERVICE_ACCOUNT_EXAMPLE)
    creds_path = tmp_path / "service_account_key.json"
    creds_path.write_text(creds_json)
    creds = {"application_credentials": creds_json}

    assert data("gs://bkt") == {"bucket": "bkt"}
    assert safe("gs://bkt") == full("gs://bkt") == "gs://bkt"
    assert data("gs://bkt/pfx") == {"bucket": "bkt", "prefix": "pfx"}
    assert safe("gs://bkt/pfx") == full("gs://bkt/pfx/") == "gs://bkt/pfx"

    cfg.REQUIRE_CREDS = True
    with pytest.raises(ValueError, match="application_credentials required"):
        data("gs://bkt")
    query = f"application_credentials={creds_json}"
    assert data(f"gs://bkt?{query}") == {"bucket": "bkt", **creds}
    assert safe(f"gs://bkt?{query}") == "gs://bkt"
    assert full(f"gs://bkt?{query}") == f"gs://bkt?{query}"

    cfg.LOAD_ENV = True
    with pytest.raises(ValueError, match="application_credentials required"):
        data("gs://bkt")
    env["GOOGLE_APPLICATION_CREDENTIALS"] = creds_json
    assert data("gs://bkt") == {"bucket": "bkt", **creds}
    env["GOOGLE_APPLICATION_CREDENTIALS"] = str(creds_path)
    assert data("gs://bkt") == {"bucket": "bkt", **creds}

    with pytest.raises(ValueError, match="Input should be 'gs'"):
        data("foo://")
    with pytest.raises(ValueError, match="at least 3 characters"):
        data("gs://")
    with pytest.raises(AssertionError, match="unexpected foo in url"):
        data("gs://bkt?foo=bar")


def test_gs_config_override(cfg, env):
    conf = config.GSConfig.from_url("gs://bkt/pfx")
    assert conf.prefix == "pfx"

    conf.apply_override(config.GSConfigOverride())
    assert conf.prefix == "pfx"

    conf.apply_override(config.GSConfigOverride(prefix="abc"))
    assert conf.prefix == "abc"


def test_az_config(cfg, env):
    data, safe, full = helpers(config.AZConfig)
    domain = "blob.core.windows.net"
    access_key = (
        "J94I0uS13Cc79AvAq33Hrkt3+C40yq16IF058yQUyiM7"
        "U2qBwGJXQ2VIrLhy0gwGRWMQ2OLTpJ6C9PsEXAMPLE=="
    )
    creds = {"access_key": access_key}

    url, url_ = "az://acct/cont", f"az://acct.{domain}/cont"
    assert safe(url) == full(url) == safe(url_) == full(url_) == url_
    assert data(url) == data(url_) == {"account": f"acct.{domain}", "container": "cont"}
    url = "az://acct/cont/pfx"
    assert safe(url) == full(url) == f"az://acct.{domain}/cont/pfx"
    assert data(url) == {
        "account": f"acct.{domain}",
        "container": "cont",
        "prefix": "pfx",
    }

    cfg.REQUIRE_CREDS = True
    with pytest.raises(ValueError, match="access_key required"):
        data("az://acct/cont")
    query = f"access_key={access_key}"
    url = f"az://acct/cont?{query}"
    assert safe(url) == f"az://acct.{domain}/cont"
    assert full(url) == f"az://acct.{domain}/cont?{query}"
    assert data(url) == {"account": f"acct.{domain}", "container": "cont", **creds}

    cfg.LOAD_ENV = True
    url = "az://acct/cont"
    with pytest.raises(ValueError, match="access_key required"):
        data(url)
    env["AZURE_ACCESS_KEY"] = access_key
    assert data(url) == {"account": f"acct.{domain}", "container": "cont", **creds}

    url = "az://acct01234567890123456789/cont"
    assert data(url) == {
        "account": f"acct01234567890123456789.{domain}",
        "container": "cont",
        **creds,
    }

    with pytest.raises(ValueError, match="Input should be 'az'"):
        data("foo://")
    with pytest.raises(ValueError, match="at least 3 characters"):
        data("az://")
    with pytest.raises(ValueError, match="at least 3 characters"):
        data("az://acct")
    with pytest.raises(AssertionError, match="unexpected foo in url"):
        data("az://acct/cont?foo=bar")


def test_az_config_override(cfg, env):
    conf = config.AZConfig.from_url("az://acct/cont/pfx")
    assert conf.prefix == "pfx"

    conf.apply_override(config.AZConfigOverride())
    assert conf.prefix == "pfx"

    conf.apply_override(config.AZConfigOverride(prefix="abc"))
    assert conf.prefix == "abc"
